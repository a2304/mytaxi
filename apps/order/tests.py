from django.test import TestCase
from .models import AcceptOrder, Order


# Create your tests here.
class OrderTest(TestCase):
	
	@classmethod
	def setUpTestData(cls):
		print("setUpTestData: Run once to set up non-modified data for all class methods.")
		AcceptOrder.objects.create(driver_id=1, order_id=1, status='new')

	def setUp(self):
		print("setUp: Run once for every test method to setup clean data.")
		self.accept_order = AcceptOrder.objects.get(id=1)
		
	def test_driver_id_label(self):
		print("test_driver_id_label: Testing driver id label is correct.")
		field_label = self.accept_order._meta.get_field('driver_id').verbose_name
		self.assertEquals(field_label, 'driver id')
		
	def test_order_id_label(self):
		print("test_order_id_label: Testing order id label is correct.")
		field_label = self.accept_order._meta.get_field('order_id').verbose_name
		self.assertEquals(field_label, 'order id')

	def test_status_label(self):
		print("test_status_label: Testing status label is correct.")
		field_label = self.accept_order._meta.get_field('status').verbose_name
		self.assertEquals(field_label, 'status')
		