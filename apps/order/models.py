from django.db import models
from apps.client.models import Client
from apps.driver.models import Driver


# Create your models here.
class Order(models.Model):
    # client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="client")
    order_car_type = models.ForeignKey(Driver, default=None, on_delete=models.CASCADE)
    pick_up_location = models.CharField(max_length=64, blank=True)
    drop_location = models.CharField(max_length=64, blank=True)
    client_phone_number = models.ForeignKey(Client, default=123456789, on_delete=models.CASCADE, related_name="phone")
    created_at = models.DateTimeField(auto_now_add=True)  # zakaz berilgan vaqt
    update_at = models.DateTimeField(auto_now=True)  # zakaz o'zgartirilgan vaqt

    def __str__(self):
        return str(self.client)


class OrderStatus(models.Model):
    status = models.CharField(max_length=200)

    def __str__(self):
        return self.status


class AcceptOrder(models.Model):
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    status = models.ForeignKey(OrderStatus, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.client} >> {self.driver}  -  {self.status}"
