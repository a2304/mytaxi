from django.forms import forms

from order.models import Order


class NameForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = [
            "client"
            "order_car_type"
            "pick_up_location"
            "drop_location"
            "client_phone_number"
            "created_at"
            "update_at"
        ]
