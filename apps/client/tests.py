from django.test import TestCase
from .models import Client


# Create your tests here.
class ClientTest(TestCase):
	
	@classmethod
	def setUpTestData(cls):
		print("setUpTestData: Run once to set up non-modified data for all class methods.")
		Client.objects.create(first_name='Big', last_name='Bob', phone_number='1234567890')
	
	def setUp(self):
		print("setUp: Run once for every test method to setup clean data.")
		self.client = Client.objects.get(id=1)
	
	def test_first_name_label(self):
		print("test_first_name_label: Testing first name label is correct.")
		field_label = self.client._meta.get_field('first_name').verbose_name
		self.assertEquals(field_label, 'first name')
	
	def test_last_name_label(self):
		print("test_last_name_label: Testing last name label is correct.")
		field_label = self.client._meta.get_field('last_name').verbose_name
		self.assertEquals(field_label, 'last name')
	
	def test_phone_number_label(self):
		print("test_phone_number_label: Testing phone number label is correct.")
		field_label = self.client._meta.get_field('phone_number').verbose_name
		self.assertEquals(field_label, 'phone number')
	
	