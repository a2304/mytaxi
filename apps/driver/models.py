from django.db import models

# Create your models here.


CAR_TYPES = (
    ('matiz', 'MATIZ'),
    ('spark', 'SPARK'),
    ('nexia1', 'NEXIA1'),
    ('nexia2', 'NEXIA2'),
    ('nexia3', 'NEXIA3'),
    ('lacetti', 'LACETTI'),
    ('gentra', 'GENTRA'),
    ('cobalt', 'COBALT'),
    ('captiva', 'CAPTIVA'),

)


class Driver(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    car_type = models.CharField(max_length=15, choices=CAR_TYPES, default='matiz')
    car_number = models.CharField(max_length=10)
    phone_number = models.CharField(max_length=15)
    start_time = models.DateTimeField(auto_now_add=True)  # driverning ish boshlagan vaqti

    def __str__(self):
        return f"{self.first_name} {self.last_name}  -  {self.car_number}"
